import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() selectedUser;
  @Output() getPreviousUserEvent = new EventEmitter;
  @Output() getNextUserEvent = new EventEmitter;
  @Output() addUserEvent = new EventEmitter;
  @Output() updateUserEvent = new EventEmitter;
  @Output() deleteUserEvent = new EventEmitter;
   inputsDisable: boolean;
   addUserClicked: boolean;

  constructor() { }

  ngOnInit() {
    this.inputsDisable = true;
    this.addUserClicked = false;
  }
  getPreviousUser(){
    if(!this.inputsDisable){
      this.inputsDisable = true;
    }
    this.getPreviousUserEvent.emit()
  }
  getNextUser(){
    if(!this.inputsDisable){
      this.inputsDisable = true;
    }
    this.getNextUserEvent.emit()
  }
  updateUser(){
    this.addUserClicked = false;
    this.inputsDisable = false;
  }
  addUser(){
    this.inputsDisable = false;
    this.addUserClicked = true;
    this.selectedUser = new User();
  }
  deleteUser(){
    this.deleteUserEvent.emit();
  }
  submit(){
    if(this.addUserClicked){
      if(this.validateForm()){
        this.addUserEvent.emit(this.selectedUser);
        this.addUserClicked = false;
        this.inputsDisable = true;
      }
    } else {
      this.updateUserEvent.emit(this.selectedUser);
      this.inputsDisable = true;
    }
   
  }
  validateForm(){
    if(this.selectedUser.name !== undefined
        && this.selectedUser.surname !== undefined
        && this.selectedUser.company !== undefined
        && this.selectedUser.job_title !== undefined
        && this.selectedUser.phone !== undefined
        && this.selectedUser.email !== undefined
        ){
          return true;
        }else{
          return false;
        }
  }
}
