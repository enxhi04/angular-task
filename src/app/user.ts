export class User {
    name: string;
    surname: string;
    company: string;
    job_title: string;
    phone: number;
    email: string;
  }
  
  