import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
//fake data
  users: User[] = [
  { name: 'name1', surname: 'surname1 ', company: 'com1', job_title: 'jt1', phone: 111, email: 'name1@domain.com' },
  { name: 'name2', surname: 'surname2 ', company: 'com2', job_title: 'jt2', phone: 222, email: 'name2@domain.com' }
];

  constructor() { }
  getUsers(): Observable<User[]> {
    return of(this.users);
  }
  addUser(user){
    this.users.push(user);
  }
  updateUser(user, index){
    this.users[index] = user;
  }
  deleteUser(index){
    this.users.splice(index, 1);
  }
}
