import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-task';
  usersList : User[];
  selectedUser : User;
  selectedUserIndex

  constructor(private userService: UserService){

  }
  ngOnInit() {
    this.getUsers();
    this.selectedUserIndex = 0;
    this.selectedUser = this.usersList[this.selectedUserIndex];
  }
  getUsers(){
    this.userService.getUsers()
    .subscribe(users => this.usersList = users);
  }
  getPreviousUser(){
    if(this.selectedUserIndex > 0){
      this.selectedUserIndex--;
      this.selectedUser = this.usersList[this.selectedUserIndex];
    }
  }
  getNextUser(){
    if(this.selectedUserIndex < this.usersList.length -1 ){
      this.selectedUserIndex++;
      this.selectedUser = this.usersList[this.selectedUserIndex];
    }
  }
  addUser(user){
    this.userService.addUser(user);
    this.getUsers();
    this.selectedUserIndex = this.usersList.length - 1;
  }
  updateUser(user){
    this.userService.updateUser(user, this.selectedUserIndex);
  }
  deleteUser(){
    if(this.usersList.length !== 1){
      this.userService.deleteUser(this.selectedUserIndex);
      this.getPreviousUser();
    }
    
  }
}
